package model.data_structures;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Clase que representa la lista doblemente encadenada
 * @param <E> Tipo de los objetos que almacenar� la lista.
 */
public class DoubleLinkedList<E> extends ListaEncadenadaAbstracta<E>
{

	/**
	 *  Constante de serializaci�n
	 */
	private static final long serialVersionUID = 1L;

	/**
	 *  Nodo de lista doble que contiene al elemento
	 */
	protected NodoListaDoble<E> primerNodo;
	
	 /**
     * Construye una lista vacia
     * <b>post:< /b> se ha inicializado el primer nodo en null
     */
	public DoubleLinkedList() 
	{
		primerNodo = null;
		cantidadElementos = 0;
	}
	
	/**
     * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�mentro
     * @param nPrimero el elemento a guardar en el primer nodo
     * @throws NullPointerException si el elemento recibido es nulo
     */
	public DoubleLinkedList(E nPrimero)
	{
		if(nPrimero == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		}
		primerNodo = new NodoListaDoble<E>(nPrimero);
		cantidadElementos = 1;
	}
	
	/**
     * Agrega un elemento al final de la lista
     * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id.
     * Se actualiza la cantidad de elementos.
     * @param e el elemento que se desea agregar.
     * @return true en caso que se agregue el elemento o false en caso contrario. 
     * @throws NullPointerException si el elemento es nulo
     */
	public boolean add(E e) 
	{
		// TODO Completar seg�n la documentaci�n
		boolean agrego = false;	
		
		if (e == null)
		{
			throw new NullPointerException();
		}		
		
		if (primerNodo != null)
		{
			boolean existe = false;
			NodoListaDoble<E> actualTemp = primerNodo;
			
			while (actualTemp != null && !existe)
			{
				if (actualTemp.darElemento() == e)
				{
					existe = true;
				}
				
				actualTemp = (NodoListaDoble<E>) actualTemp.darSiguiente();
			}
			
			if (!existe)
			{				
				NodoListaDoble<E> niu = new NodoListaDoble<E>(e);
				NodoListaDoble<E> actual = primerNodo;
				
				while (actual.darSiguiente() != null)
				{
					actual = (NodoListaDoble<E>) actual.darSiguiente();
				}
				
				niu.cambiarAnterior( (NodoListaDoble<E>) actual);
				niu.cambiarSiguiente(actual.darSiguiente());
				actual.cambiarSiguiente(niu);
				cantidadElementos++;
				agrego = true;
			}
		}
		
		else
		{
			primerNodo = new NodoListaDoble<E>(e);
			cantidadElementos++;
			agrego = true;
		}

		
		return agrego;
	}

	/**
     * Agrega un elemento al final de la lista. Actualiza la cantidad de elementos.
     * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
     * @param elemento el elemento que se desea agregar.
     * @return true en caso que se agregue el elemento o false en caso contrario. 
     * @throws NullPointerException si el elemento es nulo
     */
	public void add(int index, E elemento) 
	{
		// TODO Completar seg�n la documentaci�n
		if (elemento == null)
		{
			throw new NullPointerException();
		}
		
		if (index < 0 || index > size())
		{
			throw new IndexOutOfBoundsException();
		}

		if (primerNodo != null)
		{
			NodoListaDoble<E> actualTemp = primerNodo;
			boolean existe = false;

			while (actualTemp != null && !existe)
			{
				if (actualTemp.darElemento() == elemento)
				{
					existe = true;
				}
				
				actualTemp = (NodoListaDoble<E>) actualTemp.darSiguiente();
			}

			if (!existe)
			{
				if (index == 0)
				{
					NodoListaDoble<E> niu = new NodoListaDoble<E>(elemento);
					niu.cambiarSiguiente(primerNodo);
					niu.cambiarAnterior(null);
					primerNodo = niu;
					cantidadElementos++;
				}
				
				else if (index == size())
				{
					NodoListaDoble<E> niu = new NodoListaDoble<E>(elemento);
					NodoListaDoble<E> finall = primerNodo;
					
					while (finall.darSiguiente() != null)
					{
						finall = (NodoListaDoble<E>) finall.darSiguiente();
					}
					
					niu.cambiarAnterior(finall);
					niu.cambiarSiguiente(null);
					finall.cambiarSiguiente(niu);
					cantidadElementos++;
				}
				
				else
				{
					int i = 1;
					NodoListaDoble<E> actual = primerNodo;
					boolean inserto = false;
					
					while (actual != null && !inserto)
					{
						if (index == i)
						{
							NodoListaDoble<E> element = new NodoListaDoble<E>(elemento);
							element.cambiarSiguiente(actual.darSiguiente());
							element.cambiarAnterior(actual);
							actual.cambiarSiguiente(element);
							cantidadElementos++;
							inserto = true;
						}

						actual = (NodoListaDoble<E>) actual.darSiguiente();
						i++;
					}
				}
			}
		}
		
		else
		{
			primerNodo = new NodoListaDoble<E>(elemento);
			cantidadElementos++;
		}
	}

	/**
	 * M�todo que retorna el iterador de la lista.
	 * @return el iterador de la lista.
	 */
	public ListIterator<E> listIterator() 
	{
		return new IteradorLista<E>((NodoListaDoble<E>) primerNodo);
	}

	/**
	 * M�todo que retorna el iterador de la lista desde donde se indica.
	 * @param index �ndice desde se quiere comenzar a iterar.
	 * @return el iterador de la lista.
	 * @throws IndexOutOfBoundsException si index < 0 o index >= size()
	 */
	public ListIterator<E> listIterator(int index) 
	{
		if(index< 0 || index >= size())
			throw new IndexOutOfBoundsException("El �ndice buscado est� por fuera de la lista.");
		return new IteradorLista<E>((NodoListaDoble<E>) darNodo(index));
	}

	/**
     * Elimina el nodo que contiene al objeto que llega por par�metro.
     * Actualiza la cantidad de elementos.
     * @param objeto el objeto que se desea eliminar. objeto != null
     * @return true en caso que exista el objeto y se pueda eliminar o false en caso contrario
     */
	@SuppressWarnings("unchecked")
	public boolean remove(Object o) 
	{
		// TODO Completar seg�n la documentaci�n
		boolean elimino = false;
		
		if ( contains(o) && primerNodo != null)
		{
			if ( ( (Object) primerNodo.darElemento() ).equals(o))
			{
				elimino = true;
				
				if (primerNodo.darSiguiente() == null)
				{
					primerNodo = null;
					cantidadElementos--;
				}
				
				else
				{
					primerNodo = (NodoListaDoble<E>) primerNodo.darSiguiente();
					cantidadElementos--;
				}
			}
			
			else
			{
				NodoListaDoble<E> actual = (NodoListaDoble<E>) primerNodo.darSiguiente();
						
				while (actual != null && !elimino)
				{
					if ( ( (Object) actual.darElemento() ).equals(o) )
					{
						actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
						cantidadElementos--;
						elimino = true;
					}
					
					else
					{
						actual = (NodoListaDoble<E>) actual.darSiguiente();
					}
				}
			}
		}
		
		return elimino;
	}

	/**
     * Elimina el nodo en la posici�n por par�metro.
     * Actualiza la cantidad de elementos.
     * @param pos la posici�n que se desea eliminar
     * @return el elemento eliminado
     * @throws IndexOutOfBoundsException si index < 0 o index >= size()
     */
	public E remove(int index) 
	{
		// TODO Completar seg�n la documentaci�n
		E retorno = null;
		
		if (index < 0 || index >= size())
		{
			throw new IndexOutOfBoundsException();
		}
		
		else
		{
			if (primerNodo != null)
			{
				primerNodo = (NodoListaDoble<E>) primerNodo;

				if (index == 0)
				{
					retorno = primerNodo.darElemento();
					
					if (primerNodo.darSiguiente() != null)
					{
						primerNodo = (NodoListaDoble<E>) primerNodo.darSiguiente();
						((NodoListaDoble<E>) primerNodo).cambiarAnterior(null);
					}
					
					else
					{
						primerNodo = null;
					}
				}
				
				else if (index == size() - 1 && index != 0)
				{
					NodoListaDoble<E> anterior = (NodoListaDoble<E>) primerNodo;
					NodoListaDoble<E> actual = (NodoListaDoble<E>) primerNodo.darSiguiente();
					
					int i = 1;
					while (i != index)
					{
						anterior = actual;
						actual = (NodoListaDoble<E>) actual.darSiguiente();
						i++;
					}
					
					anterior.cambiarSiguiente(null);
				}
				
				else
				{
					boolean removed = false;
					
					NodoListaDoble<E> anterior = (NodoListaDoble<E>) primerNodo;
					NodoListaDoble<E> actual = (NodoListaDoble<E>) primerNodo.darSiguiente();
					NodoListaDoble<E> siguiente = (NodoListaDoble<E>) actual.darSiguiente();
					
					int i = 1;
					while (siguiente != null && !removed)
					{
						if (index == i)
						{
							anterior.cambiarSiguiente(siguiente);
							siguiente.cambiarAnterior(anterior);
							retorno = actual.darElemento();
							actual = null;
							removed = true;
						}
						
						else
						{
							anterior = actual;
							actual = siguiente;
							siguiente = (NodoListaDoble<E>) siguiente.darSiguiente();
						}
						i++;
					}
				}
			}
		}
		
		return retorno;
	}

	/**
     * Deja en la lista s�lo los elementos que est�n en la colecci�n que llega por par�metro.
     * Actualiza la cantidad de elementos
     * @param coleccion la colecci�n de elementos a mantener. coleccion != null
     * @return true en caso que se modifique (eliminaci�n) la lista o false en caso contrario
     */
	public boolean retainAll(Collection<?> c) 
	{
		// TODO Completar seg�n la documentaci�n
		boolean modifico = false;
		int sizePrimero = cantidadElementos;
		
		if (primerNodo != null)
		{
			if (primerNodo.darSiguiente() != null)
			{		
				NodoListaDoble<E> anterior = (NodoListaDoble<E>) primerNodo;
				NodoListaDoble<E> actual = (NodoListaDoble<E>) primerNodo.darSiguiente();
				
				while (!c.contains(anterior) && anterior != null)
				{
					primerNodo = actual;
					anterior = actual;
					actual = (NodoListaDoble<E>) actual.darSiguiente();
					cantidadElementos--;
				}
				
				while (actual != null)
				{
					if (!c.contains(actual))
					{
						cantidadElementos--;
												
						if (actual.darSiguiente() != null)
						{
							anterior.cambiarSiguiente(actual.darSiguiente());
							((NodoListaDoble<E>) actual.darSiguiente()).cambiarAnterior(anterior);
							actual = (NodoListaDoble<E>) actual.darSiguiente();
						}
						
						else
						{
							anterior.cambiarSiguiente(null);
						}
					}
					else
					{
						anterior = actual;
						actual = (NodoListaDoble<E>) actual.darSiguiente();
					}
				}
			}
			
			else
			{
				if (!c.contains(primerNodo))
				{
					primerNodo = null;
					cantidadElementos--;
				}
			}
		}
		
		if (sizePrimero != cantidadElementos)
		{
			modifico = true;
		}
		
		return true;
	}

	 /**
     * Crea una lista con los elementos de la lista entre las posiciones dadas
     * @param inicio la posici�n del primer elemento de la sublista. Se incluye en la sublista
     * @param fin la posici�n del �tlimo elemento de la sublista. Se excluye en la sublista
     * @return una lista con los elementos entre las posiciones dadas
     * @throws IndexOutOfBoundsException Si inicio < 0 o fin >= size() o fin < inicio
     */
	public List<E> subList(int inicio, int fin) 
	{
		// TODO Completar seg�n la documentaci�n
		List<E> sub = new ArrayList<E>();

		if (inicio < 0 || fin > size() || fin < inicio)	
		{
			throw new IndexOutOfBoundsException();
		}

		if (primerNodo != null)
		{
			if (primerNodo.darSiguiente() != null)
			{
				E[] array = (E[]) toArray();

				for (int i = inicio; i < fin; i++)
				{
					sub.add(array[i]);
				}
			}

			else
			{
				sub.add(primerNodo.darElemento());
			}
		}

		return sub;
	}

}
