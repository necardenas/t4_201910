package model.data_structures;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class LinkedList<E> extends ListaEncadenadaAbstracta<E>
{

	/**
	 * Constante de serializaci�n.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Construye la lista vac�a.
	 * <b>post: </b> Se ha inicializado el primer nodo en null
	 */
	public LinkedList() 
	{
		//TODO Completar el m�todo de acuerdo a la documentaci�n.
		primerNodo = null;
		cantidadElementos = 0;
	}
	
    /**
     * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�metro. Actualiza el n�mero de elementos.
     * @param nPrimero el elemento a guardar en el primer nodo
     * @throws NullPointerException si el elemento recibido es nulo
     */
	public LinkedList(E nPrimero)
	{
		//TODO Completar el m�todo de acuerdo a la documentaci�n.
		if (nPrimero == null)
		{
			throw new NullPointerException();
		}
		else
		{
			primerNodo = new NodoListaSencilla<E>(nPrimero);
			cantidadElementos++;
			
			NodoListaSencilla<E> actual = primerNodo;
			int i = 0;
			while (actual != null)
			{
				i++;
				actual = actual.darSiguiente();
			}
			cantidadElementos += i;
		}
	}

    /**
     * Agrega un elemento al final de la lista, actualiza el n�mero de elementos.
     * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
     * @param elem el elemento que se desea agregar.
     * @return true en caso que se agregue el elemento o false en caso contrario. 
     * @throws NullPointerException si el elemento es nulo
     */
	public boolean add(E elemento) 
	{
		// TODO Completar seg�n la documentaci�n
		if (elemento == null)
		{
			throw new NullPointerException();
		}

		boolean agrego = false;
		
		if (primerNodo != null)
		{
			boolean existe = false;
			NodoListaSencilla<E> actualTemp = primerNodo;
			
			while (actualTemp != null && !existe)
			{
				if (actualTemp.darElemento() == elemento)
				{
					existe = true;
				}
				
				actualTemp = actualTemp.darSiguiente();
			}
			
			if (!existe)
			{
				NodoListaSencilla<E> niu = new NodoListaSencilla<E>(elemento);
				NodoListaSencilla<E> actual = primerNodo;
				
				while (actual.darSiguiente() != null)
				{
					actual = actual.darSiguiente();
				}
				
				actual.cambiarSiguiente(niu);
				cantidadElementos++;
				agrego = true;
			}
		}
		
		else
		{
			primerNodo = new NodoListaSencilla<E>(elemento);
			cantidadElementos++;
			agrego = true;
		}

		return agrego;
	}

	/**
    * Agrega un elemento en la posici�n dada de la lista. Todos los elementos siguientes se desplazan.
    * Actualiza la cantidad de elementos.
    * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
    * @param pos la posici�n donde se desea agregar. Si pos es igual al tama�o de la lista se agrega al final
    * @param elem el elemento que se desea agregar
    * @throws IndexOutOfBoundsException si el inidice es < 0 o > size()
    * @throws NullPointerException Si el elemento que se quiere agregar es null.
    */
	public void add(int index, E elemento) 
	{
		// TODO Completar seg�n la documentaci�n
		if (elemento == null)
		{
			throw new NullPointerException();
		}
		
		if (index < 0 || index > size())
		{
			throw new IndexOutOfBoundsException();
		}

		if (primerNodo != null)
		{
			NodoListaSencilla<E> actualTemp = primerNodo;
			boolean existe = false;

			while (actualTemp != null && !existe)
			{
				if (actualTemp.darElemento() == elemento)
				{
					existe = true;
				}
				actualTemp = actualTemp.darSiguiente();
			}

			if (!existe)
			{
				if (index == 0)
				{
					NodoListaSencilla<E> niu = new NodoListaSencilla<E>(elemento);
					niu.cambiarSiguiente(primerNodo);
					primerNodo = niu;
					cantidadElementos++;
				}
				
				else if (index == size())
				{
					NodoListaSencilla<E> niu = new NodoListaSencilla<E>(elemento);
					NodoListaSencilla<E> finall = primerNodo;
					
					while (finall.darSiguiente() != null)
					{
						finall = finall.darSiguiente();
					}
					
					finall.cambiarSiguiente(niu);
					cantidadElementos++;
				}
				
				else
				{
					int i = 1;
					NodoListaSencilla<E> actual = primerNodo;
					boolean inserto = false;
					
					while (actual != null && !inserto)
					{
						if (index == i)
						{
							NodoListaSencilla<E> element = new NodoListaSencilla<E>(elemento);
							element.cambiarSiguiente(actual.darSiguiente());
							actual.cambiarSiguiente(element);
							cantidadElementos++;
							inserto = true;
						}

						actual = actual.darSiguiente();
						i++;
					}
				}
			}
		}
		
		else
		{
			primerNodo = new NodoListaSencilla<E>(elemento);
			cantidadElementos++;
		}
	}

	@Deprecated
	public ListIterator<E> listIterator() 
	{
		throw new UnsupportedOperationException ();
	}

	@Deprecated
	public ListIterator<E> listIterator(int index) 
	{
		throw new UnsupportedOperationException ();
	}

	
	/**
     * Elimina el nodo que contiene al objeto que llega por par�metro. Actualiza el n�mero de elementos.
     * @param objeto el objeto que se desea eliminar. objeto != null
     * @return true en caso que exista el objeto y se pueda eliminar o false en caso contrario
     */
	@SuppressWarnings("unchecked")
	public boolean remove(Object objeto) 
	{
		//TODO Completar seg�n la documentaci�n
		boolean elimino = false;
		
		if ( contains(objeto) && primerNodo != null)
		{
			if ( ( (Object) primerNodo.darElemento() ).equals(objeto))
			{
				elimino = true;
				
				if (primerNodo.darSiguiente() == null)
				{
					primerNodo = null;
					cantidadElementos--;
				}
				
				else
				{
					primerNodo = primerNodo.darSiguiente();
					cantidadElementos--;
				}
			}
			
			else
			{
				NodoListaSencilla<E> anterior = primerNodo;
				NodoListaSencilla<E> actual = primerNodo.darSiguiente();
						
				while (actual != null && !elimino)
				{
					if ( ( (Object) actual.darElemento() ).equals(objeto) )
					{
						anterior.cambiarSiguiente(actual.darSiguiente());
						cantidadElementos--;
						elimino = true;
					}
					
					else
					{
						anterior = actual;
						actual = actual.darSiguiente();
					}
				}
			}
		}
		
		return elimino;
	}

	/**
     * Elimina el nodo en la posici�n por par�metro. Actualiza la cantidad de elementos.
     * @param pos la posici�n que se desea eliminar
     * @return el elemento eliminado
     * @throws IndexOutOfBoundsException si pos < 0 o pos >= size()
     */
	public E remove(int pos) 
	{
		//TODO Completar seg�n la documentaci�n
		E borrado = null;
		
		if (pos < 0 || pos >= size())
		{
			throw new IndexOutOfBoundsException();
		}
		
		else
		{	
			if (primerNodo != null)
			{	
				if (pos == 0)
				{
					borrado = primerNodo.darElemento();
					
					if (primerNodo.darSiguiente() == null)
					{
						primerNodo = null;
					}
					
					else
					{
						primerNodo = primerNodo.darSiguiente();
					}
					
					cantidadElementos--;
				}
				
				else
				{
					boolean borro = false;
					int i = 1;
					NodoListaSencilla<E> anterior = primerNodo;
					NodoListaSencilla<E> actual = primerNodo.darSiguiente();
					
					while (actual != null && !borro)
					{
						if (pos == i)
						{
							borrado = actual.darElemento();
							anterior.cambiarSiguiente(actual.darSiguiente());
							cantidadElementos--;
							borro = true;
						}
						
						else
						{
							anterior = actual;
							actual = actual.darSiguiente();
						}

						i++;
					}
				}
			}

			return borrado;
		}
	}

	/**
	 * Deja en la lista solo los elementos que est�n en la colecci�n que llega por par�metro. La cantidad de elementos se actualiza.
	 * @param coleccion la colecci�n de elementos a mantener. coleccion != null
	 * @return true en caso que se modifique (eliminaci�n) la lista o false en caso contrario
	 */
	public boolean retainAll(Collection<?> coleccion) 
	{
		//TODO Completar seg�n la documentaci�n
		boolean modifico = false;
		int sizePrimero = cantidadElementos;

		if (primerNodo != null)
		{			
			while (!coleccion.contains(primerNodo) && primerNodo != null)
			{
				primerNodo = primerNodo.darSiguiente();
				cantidadElementos--;
			}
			
			if (primerNodo != null)
			{
				if (primerNodo.darSiguiente() != null)
				{
					NodoListaSencilla<E> anterior = primerNodo;
					NodoListaSencilla<E> actual = primerNodo.darSiguiente();
					while (actual != null)
					{
						if (!coleccion.contains(actual.darElemento()))
						{
							NodoListaSencilla<E> temp = actual.darSiguiente();

							anterior.cambiarSiguiente(actual.darSiguiente());

							actual = temp.darSiguiente();
							cantidadElementos--;
						}

						else
						{
							anterior = actual;
							actual = actual.darSiguiente();
						}
					}
				}				
			}
		}			

		if (sizePrimero != cantidadElementos)
		{
			modifico = true;
		}


		return modifico;
	}

	 /**
     * Crea una lista con los elementos de la lista entre las posiciones dadas
     * @param inicio la posici�n del primer elemento de la sublista. Se incluye en la sublista
     * @param fin la posici�n del �tlimo elemento de la sublista. Se excluye en la sublista
     * @return una lista con los elementos entre las posiciones dadas
     * @throws IndexOutOfBoundsException Si inicio < 0 o fin >= size() o fin < inicio
     */
	public List<E> subList(int inicio, int fin) 
	{
		//TODO Completar seg�n la documentaci�n
		List<E> sub = new ArrayList<E>();
		
		if (inicio < 0 || fin > size() || fin < inicio)	
		{
			throw new IndexOutOfBoundsException();
		}

		if (primerNodo != null)
		{
			if (primerNodo.darSiguiente() != null)
			{
				E[] array = (E[]) toArray();

				for (int i = inicio; i < fin; i++)
				{
					sub.add(array[i]);
				}
			}

			else
			{
				sub.add(primerNodo.darElemento());
			}
		}
		
		return sub;
	}
	
	
	public int getSize()
	{
		if (primerNodo == null)
			return 0;
		
		else
		{
			if (primerNodo.siguiente == null)
				return 1;
			
			else
			{
				int returno = 1;
				NodoListaSencilla<E> actual = primerNodo;
				
				while (actual.darSiguiente() != null)
				{
					returno++;
					actual = actual.darSiguiente();
				}
				
				return returno;
			}
		}
	}
}
