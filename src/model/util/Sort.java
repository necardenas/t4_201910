package model.util;

import model.vo.VOMovingViolations;

public class Sort 
{
	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 * @author Robert Sedgewick
	 * @author Kevin Wayne
	 */
	public static <E> void ordenarShellSort( Comparable<E>[ ] datos ) 
	{
		// TODO implementar el algoritmo ShellSort
		int n = datos.length;

		// 3x+1 increment sequence:  1, 4, 13, 40, 121, 364, 1093, ... 
		int h = 1;
		while (h < n/3) 
			h = 3*h + 1; 

		while (h >= 1) 
		{
			// h-sort the array
			for (int i = h; i < n; i++) 
			{
				for (int j = i; j >= h && less(datos[j], datos[j-h]); j -= h) 
				{
					exchange(datos, j, j-h);
				}
			}
			h /= 3;
		}
	}
	
	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param <E>
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static <E> void mergeSort( Comparable<E>[ ] datos, int n) 
	{
		// TODO implementar el algoritmo MergeSort
		if (n<2)
		{
			return;
		}
		
		int mid = n/2;
		Comparable[] left = new Comparable[mid];
		Comparable[] right = new Comparable[n - mid];
		
		for (int i = 0; i < mid; i++)
		{
			left[i] = datos[i];
		}
		
		for (int i = mid; i < n; i++)
		{
			right[i - mid] = datos[i];
		}
		
		mergeSort(left, mid);
		mergeSort(right, n - mid);
		
		merge(datos, left, right, mid, n - mid);
	}

	private static <E> void merge(Comparable<E>[] datos, Comparable<E>[] l, Comparable<E>[] r, 
							 int left, int right) 
	{
		int i = 0; int j = 0; int k = 0;
		
		while (i < left && j < right)
		{
			if (less(l[i], r[j]))
			{
				datos[k++] = l[i++];
			}
			else
			{
				datos[k++] = r[j++];
			}
		}
		
		while (i < left)
		{
			datos[k++] = l[i++];
		}
		
		while (j < right)
		{
			datos[k++] = r[j++];
		}
	}
	
	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 * @author Robert Sedgewick
	 * @author Kevin Wayne
	 */
	public static <E> void ordenarQuickSort( Comparable<E>[ ] datos ) 
	{
		// TODO implementar el algoritmo QuickSort
		StdRandom.shuffle(datos);
		ordenarQuickSort(datos, 0, datos.length - 1);
	}
	
	public static <E> void ordenarQuickSort( Comparable<E>[] a, int lo, int hi )
	{
		if (hi <= lo) return;
        int j = partition(a, lo, hi);
        ordenarQuickSort(a, lo, j-1);
        ordenarQuickSort(a, j+1, hi);
	}
	
	public static <E> int partition(Comparable<E>[] a, int lo, int hi)
	{
		int i = lo;
		int j = hi + 1;
		Comparable v = a[lo];
		while (true) 
		{ 

			// find item on lo to swap
			while (less(a[++i], v)) {
				if (i == hi) break;
			}

			// find item on hi to swap
			while (less(v, a[--j])) {
				if (j == lo) 
					break;      // redundant since a[lo] acts as sentinel
			}

			// check if pointers cross
			if (i >= j) 
				break;

			exchange(a, i, j);
		}

		// put partitioning item v at a[j]
		exchange(a, lo, j);

		// now, a[lo .. j-1] <= a[j] <= a[j+1 .. hi]
		return j;
	}
	
	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(Comparable v, Comparable w)
	{
		if (v.compareTo(w) > 0)
		{
			return false;
		}
		else if (v.compareTo(w) < 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static <E> void exchange( Comparable<E>[] datos, int i, int j)
	{
		// TODO implementar
		Comparable temp = datos[i];
		datos[i] = datos[j];
		datos[j] = temp;
	}

}
