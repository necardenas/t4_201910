package model.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Representation of a MovingViolation object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations> {

	// TODO Definir los atributos de una infraccion
	
	private int objectId;

	private String location;

	private int adressId;
	
	private int streetSegId;

	private double xCord;

	private double yCord;

	private String ticketType;

	private int fineamt;

	private double totalPaid;

	private int penalty1;

	private String accidentIndicator;

	private String ticketIssueDate;

	private String violationCode;

	private String violationDesc;
	
	/**
	 * Metodo constructor
	 */
	public VOMovingViolations( int objectId, String location, int adress, int streetSegId, double xCord, double yCord,
			String ticketType, int fineamt, double totalPaid, int penalty1, String accidentIndicator,
			String ticketIssueDate, String violationCode, String violationDesc )
	{
		this.objectId = objectId;
		this.location = location;
		this.adressId = adress;
		this.streetSegId = streetSegId;
		this.xCord = xCord;
		this.yCord = yCord;
		this.ticketType = ticketType;
		this.fineamt = fineamt;
		this.totalPaid = totalPaid;
		this.penalty1 = penalty1;
		this.accidentIndicator = accidentIndicator;
		this.ticketIssueDate = ticketIssueDate;
		this.violationCode = violationCode;
		this.violationDesc = violationDesc;
	}	
	
	/**
	 * @return id - Identificador unico de la infraccion
	 */
	public int objectId() {
		// TODO Auto-generated method stub
		return objectId;
	}	
	
	
	/**
	 * @return location - Direccion en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infraccion .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketType;
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente paga el que recibio la infraccion en USD.
	 */
	public double getTotalPaid() {
		// TODO Auto-generated method stub
		return totalPaid;
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicator;
	}
		
	/**
	 * @return description - Descripcion textual de la infraccion.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDesc;
	}

	@Override
	public int compareTo(VOMovingViolations o)
	{
		// TODO implementar la comparacion "natural" de la clase
		int c = 0;
		
		try
		{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
			Date d1 = sdf.parse(ticketIssueDate);
			Date d2 = sdf.parse(o.ticketIssueDate);
			c = d1.compareTo(d2);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		if (c > 0)
		{
			return 1;
		}

		else if (c < 0)
		{
			return -1;
		}

		else
		{
			if ( objectId > o.objectId() )
			{
				return 1;
			}

			else if (objectId < o.objectId())
			{
				return -1;
			}

			else
			{
				return 0;
			}
		}
	}
	
	public String toString()
	{
		// TODO Convertir objeto en String (representacion que se muestra en la consola)
		return objectId + " " + location + " " + adressId + " " + streetSegId + " " + xCord + " " + yCord + " " + ticketType + " " + fineamt + " " + totalPaid + " " + penalty1 + " " + accidentIndicator + " " + ticketIssueDate + " " + violationCode + " " + violationDesc;
	}
}
